/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anallizadorlexicosintactico;

import models.Resultado;
import common.Common;
import java.io.IOException;
import java.util.ArrayList;
import models.PalabraReservada;

/**
 *
 * @author Zaira Esparza
 */
public class AnallizadorLexicoSintactico {

    AnalizadorLexico al = new AnalizadorLexico();

    Common common = new Common();
    
    int posicion_cadena = 0;
    Tablas tablas = new Tablas();
    Pila<String> pila = new Pila<>();
    String programa = "";
    ArrayList<String> simbolos = new ArrayList(), palabrasReservadas = new ArrayList();
    ArrayList<Simbolo> array_list_simbolos = new ArrayList();
    int numero_identificador = 500;

    boolean validacion_identificados;
    public AnallizadorLexicoSintactico() throws IOException {
        programa = LeerArchivo();
    }

    public String LeerArchivo() throws IOException {
        String codigo = "";
        try {
            codigo = common.leerArchivo("codigo.txt");
        } catch (Exception e) {
        }
        return codigo;
    }

    public int predict(String x, String a) {
        int posicion_x = -1;
        int posicion_a = -1;
        int prediccion = 0;

        posicion_x = common.inArray(tablas.no_terminales, x);
        posicion_a = common.inArray(tablas.terminal, a);

        if (posicion_a >= 0 && posicion_x >= 0) {
            prediccion = tablas.matriz_predictiva[posicion_x][posicion_a];
        }
        return prediccion;
    }

    public String[] product(int prediction) {
        return tablas.derecha[prediction].split(" ");
    }

    public void analizar() {
        PalabraReservada ultimaPalabraReservada = new PalabraReservada();
        Resultado token_entrada = scanner(ultimaPalabraReservada);
        String x = "inicio";
        pila.insertar(x);
        boolean error = false;

        String a = token_entrada.getLexema();
        switch (token_entrada.getAtributo()) {
            case 307: //Si el token es un identificador
                a = "id";
                validacion_identificados = false;
                for (int i = array_list_simbolos.size() - 1; i > 0; i--) {
                    Simbolo array_list_simbolo = array_list_simbolos.get(i);
                    if(array_list_simbolo.getLexema() == token_entrada.getLexema()){
                        validacion_identificados = true;
                        array_list_simbolos.add(new Simbolo(array_list_simbolo.getLexema(), array_list_simbolo.getToken(), array_list_simbolo.getTipo(), token_entrada.getAtributo(), array_list_simbolo.getRepeticiones() + 1));
                        break;
                    }
                }
                if(!validacion_identificados){
                    array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), numero_identificador, 1));
                    numero_identificador++;
                }
                break;
            case 305: //Si el token es un entero
                a = "enteros";
                array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                break;
            case 306: //Si el token es un decimal
                a = "reales";
                array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                break;
            case -1:
                System.out.println("[01] Error Lexico");
                error = true;
                break;
            default:
                if (token_entrada.getAtributo() >= 300 && token_entrada.getAtributo() <= 304) {
                    ultimaPalabraReservada = al.isPalabraReservada(token_entrada.getLexema());
                    if(!palabrasReservadas.contains(token_entrada.getLexema())){
                        array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                        palabrasReservadas.add(token_entrada.getLexema());
                    }
                } else if (token_entrada.getAtributo() >= 32 && token_entrada.getAtributo() <= 63) {
                    array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                    if(!simbolos.contains(token_entrada.getLexema())){
                        simbolos.add(token_entrada.getLexema());
                    }
                }
        }
        
        int prediction = 0;

        while(pila.cantidad() != 0 && !error) {
            if (common.inArray(tablas.no_terminales, x) >= 0) {
                prediction = predict(x, a);
                if (prediction != 0) {
                    //Remplazar x mientras 
                    String[] derecha = product(prediction);

                    pila.extraer();
                    for (int i = derecha.length - 1; i >= 0; i--) {
                        x = derecha[i];
                        pila.insertar(x);
                    }
                } else {
                    System.out.println("[01]Error de sintaxis");
                    error = true;
                }
            } else {
                if ("ε".equals(x)) {
                    pila.extraer();
                    x = pila.extraer();
                    if (posicion_cadena != programa.length()) {
                        pila.insertar(x);
                    }
                } else {
                    if (x.equals(a)) {
                        pila.extraer();
                        token_entrada = scanner(ultimaPalabraReservada);
                        a = token_entrada.getLexema();
                        switch (token_entrada.getAtributo()) {
                            case 307: //Si el token es un identificador
                                a = "id";
                                validacion_identificados = false;
                                for (int i = array_list_simbolos.size() - 1; i > 0; i--) {
                                    Simbolo array_list_simbolo = array_list_simbolos.get(i);
                                    if(array_list_simbolo.getLexema().equals(token_entrada.getLexema())){
                                        validacion_identificados = true;
                                        array_list_simbolos.add(new Simbolo(array_list_simbolo.getLexema(), array_list_simbolo.getToken(), array_list_simbolo.getTipo(), token_entrada.getAtributo(), array_list_simbolo.getRepeticiones() + 1));
                                        break;
                                    }
                                }
                                if(!validacion_identificados){
                                    array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), numero_identificador, 1));
                                    numero_identificador++;
                                }
                                break;
                            case 305: //Si el token es un entero
                                a = "enteros";
                                array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                                break;
                            case 306: //Si el token es un decimal
                                a = "reales";
                                array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                                break;
                            case -1:
                                System.out.println("[02]Error lexico");
                                error = true;
                            default:
                                if (token_entrada.getAtributo() >= 300 && token_entrada.getAtributo() <= 304) {
                                    ultimaPalabraReservada = al.isPalabraReservada(token_entrada.getLexema());
                                    if(!palabrasReservadas.contains(token_entrada.getLexema())){
                                        array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                                        palabrasReservadas.add(token_entrada.getLexema());
                                    }
                                } else if (
                                        (token_entrada.getAtributo() >= 32 && token_entrada.getAtributo() <= 63) || 
                                        (token_entrada.getAtributo() == 123) ||
                                        (token_entrada.getAtributo() == 125)) {
                                    array_list_simbolos.add(new Simbolo(token_entrada.getLexema(), token_entrada.getToken(), token_entrada.getTipo(), token_entrada.getAtributo(), 0));
                                    if(!simbolos.contains(token_entrada.getLexema())){
                                        simbolos.add(token_entrada.getLexema());
                                    }
                                }
                        }

                        x = pila.extraer();
                        pila.insertar(x);
                    } else {
                        error = true;
                        System.out.println("[02]Error de sintaxis");
                    }
                }
            }
        }
        if(error){
            System.out.println("Hubo un problema en el lexico o en la sintaxis.");
        }else{
            System.out.println("Sintaxis y lexico correcto.");
        }
        imprimirTablas();
    }

    public Resultado scanner(PalabraReservada ultimaPalabraReservada) {
        Resultado resultado = new Resultado();
        boolean fin_token = false;
        String token = "";
        int tipo_simbolo = -1;

        for (int i = posicion_cadena; i < programa.length(); i++) {
            tipo_simbolo = al.tipoSimbolo(programa.charAt(i));
            //Se leyó solo un caracter simple
            if (token.length() == 0 && tipo_simbolo == 7) {
                token = programa.charAt(i) + "";
                resultado = al.procesar(token, ultimaPalabraReservada);
                posicion_cadena = i + 1;
                i = programa.length(); //Se finaliza el for
            } else if (i == programa.length()) { //Se revisa si es la última posición
                if (tipo_simbolo == 7) { //Se revisa si la cadena terminó con un carcater simple
                    //Si termino con un caracter no se cuenta la última posición
                    resultado = al.procesar(token, ultimaPalabraReservada);
                } else {//Significa que terminó con cualquier con algún número o letra 
                    token += programa.charAt(i) + "";
                    resultado = al.procesar(token, ultimaPalabraReservada);
                }
                posicion_cadena = i;
                i = programa.length(); //Se finaliza el for
            } else { //Significa que se leyó un caracter al final de una cadena
                switch (tipo_simbolo) { //Se revisa que tipo de dato es
                    case 5: //Se termina el token con un espacio
                        if (token.length() > 0) {
                            resultado = al.procesar(token, ultimaPalabraReservada);
                            posicion_cadena = i + 1;
                            i = programa.length(); //Se finaliza el for
                        }
                        break;
                    case 7: //Se termina el token en un signo
                        resultado = al.procesar(token, ultimaPalabraReservada);
                        posicion_cadena = i;
                        i = programa.length(); //Se finaliza el for
                        break;
                    default:
                        token += programa.charAt(i) + "";

                }
            }
        }
        return resultado;
    }

    public void imprimirTablas() {
        System.out.println("");
        System.out.format("%15s%25s%20s%10s%15s", "Lexema", "Token", "Tipo", "Valor", "Repeticiones");
        System.out.println("");
        
        Simbolo s;
        for (int i = 0; i < array_list_simbolos.size(); i++) {
            s = array_list_simbolos.get(i);
            System.out.format("%15s%25s%20s%10s%15s", s.getLexema(), s.getToken(), s.getTipo(), s.getValor(), ((s.getRepeticiones() == 0) ? "" : s.getRepeticiones()));
            System.out.println("");
        }
        /*System.out.println("Tabla de Palabras Reservadas");
        for (int i = 0; i < palabrasReservadas.length; i++) {
            System.out.println(palabrasReservadas[i]);
        }
        System.out.println("Tabla de Simbolos");
        for (int i = 0; i < simbolos.length; i++) {
            System.out.println(simbolos[i]);
        }*/
    }
}
