/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anallizadorlexicosintactico;

import models.PalabraReservada;
import models.Resultado;
import common.Common;
import java.util.ArrayList;
/**
 *
 * @author LeoNa
 */
public class AnalizadorLexico {
    Common common = new Common();
    public ArrayList<Resultado> resultados;
    String codigo;
    ArrayList<PalabraReservada> palabraReservadas;
    ArrayList<Caracter> caracteres_simples;


    public final void cargarCaracteresSimples() {
        caracteres_simples = new ArrayList();
        caracteres_simples.add(new Caracter(';', "Punto y Coma"));
        caracteres_simples.add(new Caracter('=', "Asignación"));
        caracteres_simples.add(new Caracter('+', "Operador Suma"));
        caracteres_simples.add(new Caracter('-', "Operador Resta"));
        caracteres_simples.add(new Caracter('*', "Multiplicación"));
        caracteres_simples.add(new Caracter('(', "Parentesis que abre"));
        caracteres_simples.add(new Caracter(')', "Parentesis que cierra"));
        caracteres_simples.add(new Caracter('{', "Llave que abre"));
        caracteres_simples.add(new Caracter('}', "Llave que cierra"));
        caracteres_simples.add(new Caracter(',', "Coma"));
        
    }
    
    String consultarTipoCaracter(char c){
        String tipo = "";
        for (Caracter caracter : caracteres_simples) {
            if(caracter.getLexema() == c){
                tipo = caracter.getTipo();
            }
        }
        return tipo;
    }

    //Cambiará
    public final void cargarPalabrasReservadas() {
        palabraReservadas = new ArrayList();
        palabraReservadas.add(new PalabraReservada("class", "Clase", 300));
        palabraReservadas.add(new PalabraReservada("float", "Tipo de dato", 301));
        palabraReservadas.add(new PalabraReservada("int", "Tipo de dato", 302));
        palabraReservadas.add(new PalabraReservada("read", "Lectura", 303));
        palabraReservadas.add(new PalabraReservada("write", "Escritura", 304));
    }

    public AnalizadorLexico() {
        this.resultados = new ArrayList();
        this.cargarCaracteresSimples();
        this.cargarPalabrasReservadas();
    }

    public Resultado procesar(String codigo, PalabraReservada ultimaPalabraReservada) {
        int tipo_dato_actual = -1;
        String unidad_lexica_actual = "";
        boolean unidad_lexica_valida = false;
        int tipo_inicial = -1;
        int atributo = -1;
        
        int tipo_dato = -1; //Es un error
        Resultado r = null;
        
        boolean final_cadena = false;
        for (int i = 0; codigo.length() > i; i++) {
            char c = codigo.charAt(i);
            tipo_dato_actual = tipoSimbolo(c);
            if(tipo_inicial == -1){
                tipo_inicial = tipo_dato_actual;
            }
            
            if(tipo_dato_actual == 5 || tipo_dato_actual == 7 || final_cadena){ // Es un espacio o un caracter simple
                r = new Resultado(unidad_lexica_actual); //[305 => Número natural]

                //Aquí se compara si es una palabra reservada
                PalabraReservada palabraReservada = isPalabraReservada(unidad_lexica_actual); 
                atributo = (palabraReservada.isNo_encontrada()) ? -1 : palabraReservada.getAtributo();
                if(atributo > -1){
                    r.setAtributo(atributo); //Palabras reservadas 
                    r.setTipo(palabraReservada.getTipo());
                    r.setToken("Palabra Reservada");
                }else{
                    //Si no es una palabra reservada, se revisa que sea una palabra válida
                    if(unidad_lexica_valida){
                        switch(tipo_inicial){
                            case 1: // Solo digitos
                                r.setAtributo(305); //Digito
                                r.setTipo("Int");
                                r.setToken("Enteros");
                                break;
                            case 10: // Decimales
                                r.setAtributo(306); //Decimal
                                r.setTipo("Float");
                                r.setToken("reales");
                                break;    
                            case 11:
                                r.setAtributo(307); //Identificador
                                r.setTipo(ultimaPalabraReservada.getLexema());
                                r.setToken("Identificador");
                                break;
                        }
                    }else{
                        if(tipo_inicial != 5 && tipo_inicial != 7){
                            r.setAtributo(-1); //Error
                            r.setTipo("Error");
                        }
                    }
                    if(tipo_dato_actual == 7){
                        //Se revisa si el signo actual es un caracter simple admitido
                        atributo = isCaracterValido(c);
                        if(atributo > -1){
                            r.setLexema(c); //Caracteres simples
                            r.setAtributo(atributo);
                            r.setTipo("Símbolo especial");
                            r.setToken(consultarTipoCaracter(c));
                        }else{
                            r.setLexema(c); //Error
                            r.setAtributo(-1);
                            r.setTipo("Error");
                        }
                    }
                }
                
                unidad_lexica_actual = "";
                tipo_inicial = -1;
                unidad_lexica_valida = false;  
            } else{
                unidad_lexica_actual+= c;
                switch(tipo_inicial){
                    case 1: //Digito
                        if(tipo_dato_actual == 1){
                            unidad_lexica_valida = true;
                        }else if(tipo_dato_actual == 4){
                            tipo_inicial = 10;
                            unidad_lexica_valida = false;
                        }
                        break;
                    case 10: //Digito después de punto
                        if(tipo_dato_actual == 1){
                            unidad_lexica_valida = true;
                        }else if(tipo_dato_actual == 4){
                            unidad_lexica_valida = false;
                            tipo_inicial = 12;
                        }
                        break;
                    case 13:
                        unidad_lexica_valida = false;
                        break;
                    case 2: //Mayuscula [Identificador]
                        unidad_lexica_valida = false;
                        tipo_inicial = 11;
                        break;
                    case 11: //identificador [Continuación]
                        if(tipo_dato_actual == 6 || tipo_dato_actual == 3 
                                || tipo_dato_actual == 1){
                            unidad_lexica_valida = true;
                        }else{
                            unidad_lexica_valida = false;
                            tipo_inicial = 12;
                        }
                        break;
                    default:
                }
                
                if(i == codigo.length() -1 && !final_cadena){
                    final_cadena = true;
                    i--;
                }
                
            }
        }
        
        //Retornar el tipo de dato
        return r;
    }
    
    public int isCaracterValido(char simboloComparar){
        int atributo = -1;
        for (int i = 0; i < caracteres_simples.size(); i++) {
            if(caracteres_simples.get(i).getLexema() == simboloComparar){
                atributo = Character.codePointAt(String.valueOf(
                        caracteres_simples.get(i).getLexema()), 0);
                break;
            }
        }
        return atributo;
    }
    
    public PalabraReservada isPalabraReservada(String simboloComparar){
        PalabraReservada palabraReservada = new PalabraReservada();
        int atributo = -1;
        for (int i = 0; i < palabraReservadas.size(); i++) {
            if(palabraReservadas.get(i).getLexema().equals(simboloComparar)){
                palabraReservada = palabraReservadas.get(i);
                break;
            }
        }
        return palabraReservada;
    }
    
    public int tipoSimbolo(char simbolo){
        int tipo = 0;
        int code = Character.codePointAt(String.valueOf(simbolo), 0);
        
        if(code >= 49 && code <= 57) tipo = 1; //Digito
        else if(code == 48) tipo = 13; //Digito cero
        else if(code >= 65 && code <= 90) tipo = 2; // Mayuscua
        else if(code >= 97 && code <= 122) tipo = 3;// Minuscula
        else if(code == 46) tipo = 4; //Punto
        else if(code == 32) tipo = 5; //Espacio
        else if(code == 95) tipo = 6; //Guión Bajo
        else tipo = 7;
        
        return tipo;
    }
    
}
