/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anallizadorlexicosintactico;

/**
 *
 * @author PC1
 */
class Simbolo {
    private String lexema;
    private String token;
    private String tipo;
    private int valor;
    private int repeticiones;

    public Simbolo(String lexema, String token, String tipo, int valor, int repeticiones) {
        this.lexema = lexema;
        this.token = token;
        this.tipo = tipo;
        this.valor = valor;
        this.repeticiones = repeticiones;
    }
    
    public void addRepeticion(){
        this.repeticiones++;
    }   

    public String getLexema() {
        return lexema;
    }

    public String getToken() {
        return token;
    }

    public String getTipo() {
        return tipo;
    }

    public int getValor() {
        return valor;
    }

    public int getRepeticiones() {
        return repeticiones;
    }
    
    
    
}
