/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anallizadorlexicosintactico;

/**
 *
 * @author PC1
 */
class Caracter {
    char lexema;
    String tipo;

    public Caracter(char lexema, String tipo) {
        this.lexema = lexema;
        this.tipo = tipo;
    }

    public char getLexema() {
        return lexema;
    }

    public void setLexema(char lexema) {
        this.lexema = lexema;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
