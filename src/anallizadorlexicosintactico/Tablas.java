package anallizadorlexicosintactico;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LeoNa
 */
public class Tablas {
    String [] no_terminales;
    String [] terminal;
    String [] derecha;

    int [][] matriz_predictiva;
    public Tablas() {
        no_terminales = new String[14];
        no_terminales[0] = "programa";
        no_terminales[1] = "lista_sent";
        no_terminales[2] = "sent_final";
        no_terminales[3] = "sentencia";
        no_terminales[4] = "lista_id";
        no_terminales[5] = "id_final";
        no_terminales[6] = "lista_expr";
        no_terminales[7] = "lista_exprfinal";
        no_terminales[8] = "expresion";
        no_terminales[9] = "expr_final";
        no_terminales[10] = "expr_arit";
        no_terminales[11] = "tipo";
        no_terminales[12] = "operador";
        no_terminales[13] = "inicio";
        
        terminal = new String[18];
        terminal[0]= "class";
        terminal[1]= "id";
        terminal[2]= "{";
        terminal[3]= "}";
        terminal[4]= ";";
        terminal[5]= "=";
        terminal[6]= "read";
        terminal[7]= "(";
        terminal[8]= ")";
        terminal[9]= "write";
        terminal[10]= ",";
        terminal[11]= "enteros";
        terminal[12]= "reales";
        terminal[13]= "int";
        terminal[14]= "float";
        terminal[15]= "+";
        terminal[16]= "-";
        terminal[17]= "*";
        
        derecha = new String[28];
        derecha[0] = "";
        derecha[1] = "class id { lista_sent }";
        derecha[2] = "sentencia sent_final";
        derecha[3] = "sentencia sent_final";
        derecha[4] = "ε";
        derecha[5] = "tipo lista_id ;";
        derecha[6] = "id = expresion ;";
        derecha[7] = "read ( lista_id ) ;";
        derecha[8] = "write ( lista_expr ) ;";
        derecha[9] = "id id_final";
        derecha[10] = ", id id_final";
        derecha[11] = "ε";
        derecha[12] = "expresion lista_exprfinal";
        derecha[13] = ", expresion lista_exprfinal";
        derecha[14] = "ε";
        derecha[15] = "expr_arit expr_final";
        derecha[16] = "operador expr_arit expr_final";
        derecha[17] = "ε";
        derecha[18] = "( expresion )";
        derecha[19] = "id";
        derecha[20] = "enteros";
        derecha[21] = "reales";
        derecha[22] = "int";
        derecha[23] = "float";
        derecha[24] = "+";
        derecha[25] = "-";
        derecha[26] = "*";
        derecha[27] = "programa";
        
        matriz_predictiva = new int[14][19];
        for (int i = 0; i <= 13; i++) {
            for (int j = 0; j <= 18; j++) {
                matriz_predictiva[i][j] = 0;
            }
        }
        
        //programa
        matriz_predictiva[0][0] = 1;
        
        //list_Sent
        matriz_predictiva[1][1] = 2;
        matriz_predictiva[1][6] = 2;
        matriz_predictiva[1][9] = 2;
        matriz_predictiva[1][13] = 2;
        matriz_predictiva[1][14] = 2;
        
        //sent_final
        matriz_predictiva[2][1] = 3;
        matriz_predictiva[2][3] = 4;
        matriz_predictiva[2][6] = 3;
        matriz_predictiva[2][9] = 3;
        matriz_predictiva[2][13] = 3;
        matriz_predictiva[2][14] = 3;
        
        //sentencia
        matriz_predictiva[3][1] = 6;
        matriz_predictiva[3][6] = 7;
        matriz_predictiva[3][9] = 8;
        matriz_predictiva[3][13] = 5;
        matriz_predictiva[3][14] = 5;
        
        //list_id
        matriz_predictiva[4][1] = 9;
        
        //id_final
        matriz_predictiva[5][4] = 11;
        matriz_predictiva[5][8] = 11;
        matriz_predictiva[5][10] = 10;
        
        //lista_expr
        matriz_predictiva[6][1] = 12;
        matriz_predictiva[6][7] = 12;
        matriz_predictiva[6][11] = 12;
        matriz_predictiva[6][12] = 12;
        
        //lista_exprfinal
        matriz_predictiva[7][4] = 13;
        matriz_predictiva[7][8] = 14;
        
        //expresion
        matriz_predictiva[8][1] = 15;
        matriz_predictiva[8][7] = 15;
        matriz_predictiva[8][11] = 15;
        matriz_predictiva[8][12] = 15;
        
        //expr_final
        matriz_predictiva[9][4] = 17;
        matriz_predictiva[9][8] = 17;
        matriz_predictiva[9][15] = 16;
        matriz_predictiva[9][16] = 16;
        matriz_predictiva[9][17] = 16;
        
        //expr_arit
        matriz_predictiva[10][1] = 19;
        matriz_predictiva[10][7] = 18;
        matriz_predictiva[10][11] = 20;
        matriz_predictiva[10][12] = 21;
        
        //tipo
        matriz_predictiva[11][13] = 22;
        matriz_predictiva[11][14] = 23;
        
        //operador
        matriz_predictiva[12][15] = 24;
        matriz_predictiva[12][16] = 25;
        matriz_predictiva[12][17] = 26;
        
        //inicio
        matriz_predictiva[13][0] = 27;
    }
}
