package common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import models.Resultado;


/**
 *
 * @author LeoNa
 */
public class Common {
    
    public String [] array_push(String [] strings, String s){
        String [] ses = new String[strings.length + 1];
        
        for (int i = 0; i < strings.length; i++) {
            ses[i] = strings[i];
        }
        ses[strings.length] = s;
        return ses;
    }
    
    public String leerArchivo(String archivo) throws IOException{
        String cadena;
        String resultado = "";
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            resultado += cadena;
        }
        
        b.close();
        System.out.println(resultado);
        return resultado;
    }
    
    public int inArray(String [] array, String valor){
        int posicion = -1;
        
        for (int i = 0; i < array.length; i++) {
            if(array[i].equals(valor)){
                posicion = i;
                i = array.length;
            }
        }
        
        return posicion;
    }
}
