package models;

/**
 *
 * @author LeoNa
 */
public class PalabraReservada {
    private boolean no_encontrada;
    private String lexema;
    private String tipo;
    private int atributo;

    public PalabraReservada(String lexema, String tipo, int atributo) {
        this.lexema = lexema;
        this.atributo = atributo;
        this.tipo = tipo;
        this.no_encontrada = false;
    }
    public PalabraReservada() {
        this.no_encontrada = true;
    }

    public String getLexema() {
        return lexema;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }

    public int getAtributo() {
        return atributo;
    }

    public void setAtributo(int atributo) {
        this.atributo = atributo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isNo_encontrada() {
        return no_encontrada;
    }

    public void setNo_encontrada(boolean no_encontrada) {
        this.no_encontrada = no_encontrada;
    }
    
    
    
    
}
